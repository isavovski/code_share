import app from "./app";
import db from "./db";
import http from "http";

db.connect()
    .then(() => {
        // Server Setup
        const port = process.env.PORT || 8000;
        http.createServer(app).listen(port, ()=>{
            console.log(`\x1b[32m`, `Server listening on: ${port}`, `\x1b[0m`);
        });
    })