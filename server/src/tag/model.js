import mongoose, { mongo } from 'mongoose';
import bcrypt from 'bcrypt-nodejs';

const Schema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    snippets: {
        type: Number,
        min: 0,
        default: 0
    },
})

export default mongoose.model('Tag', Schema);