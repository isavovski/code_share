import TagModel from './model';

export default {
    all: (req, res) => {
        TagModel.find()
            .then(tags => {
                res.json({success: true, tags});
            })
            .catch(err => next(err));
    }
}


export function checkTags(tagNames) {
    // check if tag names exist, create ones if necessary
    return tagNames.map(tagName => new Promise((resolve, reject) => {
        TagModel.findOne({ name: tagName })
            .then(tag => {
                if (!tag) {
                    return new TagModel({
                        name: tagName
                    }).save();
                }
                return tag;
            })
            .then(tag => resolve(tag))
            .catch(err => reject(err))
    }));
}

export function updateTags(tags, amount = 1) {
    // update the tag's number of corresponding snippets            
    return tags.map(tagId => new Promise((resolve, reject) => {
        TagModel.findOneAndUpdate(
            {_id: tagId},
            {
                $inc: { snippets: amount },
            },
            {new: true, runValidators: true},
            )
            .then(tag => {
                resolve(tag);
            })
            .catch(err => reject(err))
    }));
}
