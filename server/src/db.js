import mongoose from 'mongoose';
import config from "./config/config";

export default {
    connect: () => {
      return new Promise((resolve, reject) => {
    
        // if (process.env.NODE_ENV === 'test') {
        //   const Mockgoose = require('mockgoose').Mockgoose;
        //   const mockgoose = new Mockgoose(mongoose);
    
        //   mockgoose.prepareStorage()
        //     .then(() => {
        //       mongoose.connect(DB_URI,
        //         { useNewUrlParser: true, useCreateIndex: true })
        //         .then((res, err) => {
        //           if (err) return reject(err);
        //           resolve();
        //         })
        //     })
        // } else {
        mongoose.connect(
            config.mongoose.uri, 
            { 
                useNewUrlParser: true, 
                useCreateIndex: true, 
                useUnifiedTopology: true, 
                useFindAndModify: false 
            })
            .then((res, err) => {
                console.log("Connected to db.")
                if (err) return reject(err);
                resolve();
            });
        });
    },
    close: () => {
      return mongoose.disconnect();
    }
}