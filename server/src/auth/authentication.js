import token from '../util/token';
import UserModel from '../user/model';

export default {
    signup : (req, res, next) => {
        const { email, password, name } = req.body;
    
        if (!email || !password) {
            return res
                .status(422)
                .send({message: 'You must provide email and password.'});
        }
        UserModel.findOne({
            email: email
        })
        .then(existingUser => {
            if (existingUser) {
                return res
                    .status(422)
                    .send({message: 'Email is in use'});
            }
            const user = new UserModel({
                name,
                email: email,
                password: password
            })

            user.save()
            .then(savedUser => {
                res.json({
                    success: true,
                    user: {
                        token: token.generateToken(savedUser),
                        name: user.name,
                        id: user._id
                    }
                });
            })
            .catch((err) => {
                return next(err);
            });
        })
        .catch(err => {
            return res.status(422).send(err);
        });
    },
    
    signin: (req, res, next) => {
        const email = req.body.email;
        const password = req.body.password;
        if (!email || !password) {
            return res
                .status(422)
                .send({message: 'You must provide email and password.'});
        }
        UserModel
            .findOne({
                email: email
            }, function (err, existingUser) {
                if (err || !existingUser) {
                    return res.status(401).send(err || {message: "User Not Found"})
                }
                if (existingUser) {
                    existingUser.comparedPassword(password, function(err, good) {
                        if (err || !good) {
                            return res.status(401).send(err || {message: 'User not found'})
                        }
                        res.send({
                            success: true,
                            user: {
                                token: token.generateToken(existingUser),
                                name: existingUser.name,
                                id: existingUser._id
                            }
                        })
                    })
                }
            })
    }
}
