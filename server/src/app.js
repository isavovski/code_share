import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import Middlewares from './auth/middlewares';
import Authentication from './auth/authentication';
import UserRouter from './user/router';
import SnippetRouter from './snippet/router';
import TagRouter from './tag/router';

if(!process.env.JWT_SECRET) {
    const err = new Error('No JWT_SECRET in env variable');
    console.error(err);
}
const app = express();

// App Setup
app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Public routes
app.get('/ping', (req, res) => res.send('pong'));
app.post('/signup', Authentication.signup);
app.post('/signin', Authentication.signin);
app.use('/snippet', SnippetRouter.public);
// Private routes
app.get('/auth-ping', Middlewares.loginRequired, (req, res) => res.send('connected'));
app.use('/user', Middlewares.loginRequired, UserRouter);
app.use('/snippet', Middlewares.loginRequired, SnippetRouter.private);
app.use('/tag', Middlewares.loginRequired, TagRouter);

app.use((err, req, res, next) => {
    console.error(err);
    res.status(422).json({success: false, error: err.message});
});

export default app;