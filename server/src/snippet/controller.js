import SnippetModel from './model';
import TagModel from '../tag/model';
import { checkTags, updateTags } from '../tag/controller';

export default {
    get: (req, res, next) => {
        SnippetModel
            .find()
            .populate('tags')
            .sort({ _id: -1 })
            .then(snippets => {
                res.json({ success: true, snippets })
            })
            .catch(err => next(err));
    },

    getByUser: (req, res, next) => {
        SnippetModel
            .find({ user: req.user._id })
            .populate('tags')
            .sort({ _id: -1 })
            .then(snippets => {
                res.json({ success: true, snippets })
            })
            .catch(err => next(err));
    },

    create: (req, res, next) => {
        let { title, content, tags: tagNames = [] } = req.body;

        Promise.all(checkTags(tagNames))
            .then(tags => {
                const snippetModel = new SnippetModel({
                    title,
                    content,
                    tags: tags.map(el => el.id),
                    user: req.user._id
                });
                snippetModel.save()
                    .then(snippet => {
                        Promise.all(updateTags(snippet.tags));
                        snippet.tags = tags;
                        res.json({ success: true, snippet });
                    })
                    .catch(err => next(err));
            })
            .catch(err => next(err));
    },

    update: (req, res, next) => {
        let data = req.body;
        SnippetModel.findById(req.params.id)
            .then(oldSnippet => {
                if (!oldSnippet) {
                    next({ message: "Snippet not found." })
                    return;
                }
                if (oldSnippet.user != req.user._id) {
                    next({ message: "Unauthorized." });
                    return;
                }
                Promise.all(checkTags(data.tags || []))
                    .then(tags => {
                        if (tags.length > 0) {
                            tags = tags.map(el => el.id);
                            data = { ...data, tags };
                        }
                        SnippetModel.findByIdAndUpdate(
                            req.params.id,
                            data,
                            {
                                new: true,
                                runValidators: true
                            })
                            .then(newSnippet => {
                                // if tags are changed update the number of corresponding snippets 
                                let removedTags = oldSnippet.tags.filter(el => !newSnippet.tags.includes(el));
                                let newTags = newSnippet.tags.filter(el => !oldSnippet.tags.includes(el));
                                Promise.all([
                                    updateTags(removedTags, -1),
                                    updateTags(newTags, 1)
                                ]);

                                res.json({ success: true, snippet: newSnippet });
                            })
                            .catch(err => next(err));
                    })
                    .catch(err => next(err));
            })
            .catch(err => next(err));

    },

    like: (req, res, next) => {
        SnippetModel.findById(req.params.id)
            .then(snippet => {
                if(snippet.likes.find(userId => userId == req.user.id)) {
                    next({message: "Can't like more than once."})
                    return;
                }
                snippet.likes.push(req.user._id);
                
                SnippetModel.findByIdAndUpdate(
                    req.params.id,
                    snippet,
                    {new: true}    
                )
                    .populate("tags")
                    .then(snippet => {
                        res.json({ success: true, snippet });
                    })
                    .catch(err => next(err))
            })
    },

    delete: (req, res, next) => {
        SnippetModel.findOne({ _id: req.params.id })
            .then(snippet => {
                if (!snippet) {
                    next({ message: "Snippet not found." });
                    return;
                }
                SnippetModel.deleteOne({ _id: req.params.id })
                    .then(() => {
                        Promise.all(updateTags(snippet.tags, -1));
                        res.json({ success: true });
                    })
                    .catch(err => next(err));

            })
            .catch(err => next(err));
    }
}