import SnippetController from './controller';
import express from "express";

const publicRouter = express.Router();
publicRouter.get('/',SnippetController.get);

const privateRouter = express.Router();
privateRouter.get('/byUser',SnippetController.getByUser);
privateRouter.post('/', SnippetController.create);
privateRouter.patch('/like/:id?', SnippetController.like);
privateRouter.patch('/:id', SnippetController.update);
privateRouter.delete('/:id', SnippetController.delete);

export default {public: publicRouter, private: privateRouter};