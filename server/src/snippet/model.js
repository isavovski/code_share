import mongoose from 'mongoose';

const Schema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    user: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    tags: [{
        type: mongoose.Types.ObjectId,
        ref: 'Tag'
    }],
    likes: [{
        type: mongoose.Types.ObjectId,
        ref: 'User'
    }]

}, {timestamps: true})

export default mongoose.model('Snippet', Schema);