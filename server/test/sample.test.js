import  request from 'supertest';

const app = require('../src/app');
const conn = require('../src/db');

test('Ping Endpoints', () => {
    request(app)
    .get('/ping')
    .then(res => {
        expect(res.status).toEqual(200)
    })

})