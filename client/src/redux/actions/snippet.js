import { snippetConstants } from '../../constants';
import { snippetService } from '../services';
import { alertActions } from '.';

export const snippetActions = {
    getSome: (id) => {
        return dispatch => {
            dispatch(request());
    
            snippetService.getSome(id)
                .then(
                    snippets => { 
                        dispatch(success(snippets));
                    },
                    error => {
                        dispatch(failure(error));
                        dispatch(alertActions.error(error));
                    }
                );
        };
    
        function request() { return { type: snippetConstants.GET_SOME_REQUEST } }
        function success(snippets) { return { type: snippetConstants.GET_SOME_SUCCESS, snippets } }
        function failure(error) { return { type: snippetConstants.GET_SOME_FAILURE, error } }
    },
    
    getByUser: () => {
        return dispatch => {
            dispatch(request());
    
            snippetService.getByUser()
                .then(
                    snippets => { 
                        dispatch(success(snippets));
                    },
                    error => {
                        dispatch(failure(error));
                        dispatch(alertActions.error(error));
                    }
                );
        };
    
        function request() { return { type: snippetConstants.GET_BY_USER_REQUEST } }
        function success(snippets) { return { type: snippetConstants.GET_BY_USER_SUCCESS, snippets } }
        function failure(error) { return { type: snippetConstants.GET_BY_USER_FAILURE, error } }
    },
    
    create: (data) => {
        return dispatch => {
            dispatch(request());
            return snippetService.create(data)
                .then(
                    snippet => { 
                        dispatch(success(snippet));
                    },
                    error => {
                        dispatch(failure(error));
                        dispatch(alertActions.error(error));
                    }
                );
        };
    
        function request() { return { type: snippetConstants.CREATE_REQUEST } }
        function success(snippet) { return { type: snippetConstants.CREATE_SUCCESS, snippet } }
        function failure(error) { return { type: snippetConstants.CREATE_FAILURE, error } }
    },

    like: (snippetId) => {
        return dispatch => {
            dispatch(request());
            return snippetService.like(snippetId)
                .then(
                    snippet => { 
                        dispatch(success(snippet));
                    },
                    error => {
                        dispatch(failure(error));
                        dispatch(alertActions.error(error));
                    }
                );
        };
    
        function request() { return { type: snippetConstants.LIKE_REQUEST } }
        function success(snippet) { return { type: snippetConstants.LIKE_SUCCESS, snippet } }
        function failure(error) { return { type: snippetConstants.LIKE_FAILURE, error } }
    }
}

