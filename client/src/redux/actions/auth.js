import { userConstants } from '../../constants';
import { userService } from '../services';
import { alertActions } from '.';

export const userActions = {
    login: (username, password) => {
        return dispatch => {
            dispatch(request({ username }));
    
            userService.login(username, password)
                .then(
                    user => { 
                        dispatch(success(user));
                        window.location = "/";
                    },
                    error => {
                        dispatch(failure(error));
                        dispatch(alertActions.error(error));
                    }
                );
        };
    
        function request() { return { type: userConstants.LOGIN_REQUEST} }
        function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
        function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
    },

    register: (data) => {
        return dispatch => {
            dispatch(request());
    
            userService.register(data)
                .then(
                    user => { 
                        dispatch(success(user));
                        window.location = "/";
                    },
                    error => {
                        dispatch(failure(error));
                        dispatch(alertActions.error(error));
                    }
                );
        };
    
        function request() { return { type: userConstants.REGISTER_REQUEST} }
        function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
        function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
    },
    
    logout: () => {
        userService.logout();
        return { type: userConstants.LOGOUT };
    }
};
