import config from 'config';
import { handleResponse } from '.';

export const userService = {
    login: (username, password) => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ email:username, password })
        };
    
        return fetch(`${config.apiUrl}/signin`, requestOptions)
            .then(handleResponse)
            .then(data => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('user', JSON.stringify(data.user));
    
                return data.user;
            });
    },

    register: (data) => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        };
    
        return fetch(`${config.apiUrl}/signup`, requestOptions)
            .then(handleResponse)
            .then(data => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('user', JSON.stringify(data.user));
    
                return data.user;
            });
    },
    
    logout: () => {
        // remove user from local storage to log user out
        localStorage.removeItem('user');
    }
};

