import config from 'config';
import { handleResponse, headers } from '.';

export const snippetService = {

    getSome: (id="") => {
        const requestOptions = {
            method: 'GET',
            headers
        };

        return fetch(`${config.apiUrl}/snippet/${id}`, requestOptions)
            .then(handleResponse)
            .then(data => {
                return data.snippets;
            });
    },

    getByUser: () => {
        const requestOptions = {
            method: 'GET',
            headers
        };

        return fetch(`${config.apiUrl}/snippet/byUser`, requestOptions)
            .then(handleResponse)
            .then(data => {
                return data.snippets;
            });
    },

    create: (data) => {
        const requestOptions = {
            method: 'POST',
            headers,
            body: JSON.stringify(data)
        };

        return fetch(`${config.apiUrl}/snippet/`, requestOptions)
            .then(handleResponse)
            .then(data => {
                return data.snippet;
            });
    },

    like: (snippetId) => {
        const requestOptions = {
            method: 'PATCH',
            headers
        };

        return fetch(`${config.apiUrl}/snippet/like/${snippetId}`, requestOptions)
            .then(handleResponse)
            .then(data => {
                return data.snippet;
            });    
    }

}