import { combineReducers } from 'redux';

import { authentication } from './authentication';
import { alert } from './alert';
import { snippets } from './snippet';

const rootReducer = combineReducers({
  authentication,
  alert,
  snippets,
});

export default rootReducer;