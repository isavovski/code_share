import { snippetConstants } from '../../constants';

export function snippets(state = {}, action) {
    switch (action.type) {
        case snippetConstants.GET_SOME_REQUEST:
            return {
                loading: true
            };
        case snippetConstants.GET_SOME_SUCCESS:
            return {
                items: action.snippets
            };
        case snippetConstants.GET_SOME_FAILURE:
            return {
                error: action.error
            };

        case snippetConstants.GET_BY_USER_REQUEST:
            return {
                loading: true
            };
        case snippetConstants.GET_BY_USER_SUCCESS:
            return {
                items: action.snippets
            };
        case snippetConstants.GET_BY_USER_FAILURE:
            return {
                error: action.error
            };

        case snippetConstants.CREATE_REQUEST:
            return {
                ...state,
                loading: true
            };
        case snippetConstants.CREATE_SUCCESS:
            return {
                ...state,
                items: [action.snippet, ...state.items]
            };
        case snippetConstants.CREATE_FAILURE:
            return {
                ...state,
                error: action.error
            };

        case snippetConstants.LIKE_REQUEST:
            return {
                ...state,
                loading: true
            };
        case snippetConstants.LIKE_SUCCESS:
            let oldSnippet = state.items.findIndex(item => item._id == action.snippet._id);
            let items = state.items.slice();
            items[oldSnippet] = action.snippet;
            return {
                ...state,
                items,
                loading: false
            };
        case snippetConstants.LIKE_FAILURE:
            return {
                ...state,
                error: action.error
            };

        default:
            return state
    }
}