import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { snippetActions } from '../redux/actions';
import { Form, Input, Button, Checkbox } from 'antd';
import { Snippet } from './Snippet';


class SnippetsPage extends React.Component {
    componentDidMount() {
        this.props.dispatch(snippetActions.getSome());
    }

    renderSnippets(data) {
        if(!data || !data.length) return;
        return data.map((data, i) => 
            <Snippet 
                key={i} 
                data={data} 
            />);
    }

    render() {
        const { snippets } = this.props;
        return (
            <div className="snippet-container">
                <div className="snippets">
                    {this.renderSnippets(snippets)}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { items: snippets } = state.snippets;
    const { loggedIn } = state.authentication;
    return {
        snippets,
        loggedIn
    };
}

const connectedPage = connect(mapStateToProps)(SnippetsPage);
export { connectedPage as SnippetsPage };