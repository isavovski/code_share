import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../redux/actions';
import { Form, Input, Button, Checkbox } from 'antd';


class HomePage extends React.Component {
    componentDidMount() {
        // this.props.dispatch(userActions.getAll());
    }
    
    render() {
        return (
            <div 
                style={{
                    textAlign:"center",
                    paddingTop:"20px"
                }}
            >
                <h1>Welcome!</h1>
                <p>This is a code snippet sharing platform!</p>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage };