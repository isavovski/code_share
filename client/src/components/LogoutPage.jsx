import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { userActions } from '../redux/actions';

class Logout extends Component {
    componentDidMount() {
        this.props.dispatch(userActions.logout());
    }
    render() {
        return (
            <Redirect to="/login" />
        );
    }
}

export default connect(null)(Logout)