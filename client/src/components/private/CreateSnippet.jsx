import React, { useState } from 'react';
import { Modal, Button } from 'antd';
import { connect } from 'react-redux';
import { Form, Input, InputNumber } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Select } from 'antd';
import { snippetActions } from '../../redux/actions';
import { snippetConstants } from '../../constants';

const { Option } = Select;

function CreateSnippet(props) {
    const [isFormVisible, setIsFormVisible] = useState(false);
    const { success, dispatch } = props;
    const showForm = () => {
        setIsFormVisible(true);
    };

    const handleSubmit = (values) => {
        dispatch(snippetActions.create(values)).then(() => {
            setIsFormVisible(false);
        });
    };

    const handleCancel = () => {
        setIsFormVisible(false);
    };

    return (
        <>
            {
                isFormVisible &&
                <CreateSnippetForm
                    handleSubmit={handleSubmit}
                    handleCancel={handleCancel}
                />
                ||
                <Button type="primary" block onClick={showForm}>
                    Create new Snippet
                </Button>
            }
        </>
    );
};

const connectedPage = connect(null)(CreateSnippet);
export { connectedPage as CreateSnippet };


export const CreateSnippetForm = (props) => {

    const validateMessages = {
        required: '${label} is required!'
    };

    const layout = {
        labelCol: { span: 4 }
    };
    return (
        <Form
            {...layout}
            name="nest-messages"
            onFinish={props.handleSubmit}
            validateMessages={validateMessages}
            style={{ textAlign: "start" }}
        >

            <Form.Item
                name='title'
                label="Title"
                rules={[
                    {
                        required: true,
                    },
                ]}
            >
                <Input style={{ width: '40vw' }} />
            </Form.Item>

            <Form.Item
                name="content"
                label="Content"
                rules={[
                    {
                        required: true,
                    },
                ]}
            >
                <Input.TextArea style={{ width: '40vw' }} rows={10} />
            </Form.Item>

            <Form.Item
                name="tags"
                label="Tags"
            >
                <Select
                    mode="tags"
                    allowClear
                    style={{ width: '40vw' }}
                    placeholder="Tags"
                />
            </Form.Item>

            <Form.Item
                style={{ textAlign: "center" }}
            >
                <Button type="primary" htmlType="submit">
                    Submit
                    </Button>
                <p />
                <Button type="secondary" onClick={props.handleCancel}>
                    Cancel
                    </Button>
            </Form.Item>
        </Form>
    );
};