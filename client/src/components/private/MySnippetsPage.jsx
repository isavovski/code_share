import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { snippetActions } from '../../redux/actions';
import { Form, Input, Button, Checkbox, Alert } from 'antd';
import { Snippet } from '../Snippet';
import { CreateSnippet } from './CreateSnippet';


class MySnippetsPage extends React.Component {
    componentDidMount() {
        this.props.dispatch(snippetActions.getByUser());
    }

    renderSnippets(data) {
        if (!data || !data.length) return;
        return data.map((data, i) => 
            <Snippet key={i} 
            data={data} 
            owned={true} 
        />);
    }

    render() {
        const { error, snippets } = this.props;
        return (
            <div className="snippet-container">
                {
                    error && <Alert message={"Error " + error} type="error" showIcon />
                }
                <CreateSnippet/>
                <div className="snippets">
                    {this.renderSnippets(snippets)}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { items: snippets, error } = state.snippets;
    const { loggedIn } = state.authentication;
    return {
        snippets,
        error,
        loggedIn
    };
}

const connectedPage = connect(mapStateToProps)(MySnippetsPage);
export { connectedPage as MySnippetsPage };