import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Col, Menu, Row } from 'antd';
import { MailOutlined, AppstoreOutlined, HomeOutlined, UserOutlined } from '@ant-design/icons';
import { connect } from 'react-redux';

const { SubMenu } = Menu;
class NavMenu extends Component {
    constructor() {
        super();
        let pathname = window.location.pathname.substr(1);
        !pathname && (pathname = 'home');
        this.state = {
            current: pathname,
        };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        this.setState({
            current: e.key,
        });
    }

    render() {
        const { current } = this.state;
        const { loggedIn } = this.props;
        return (
            <Menu
                onClick={this.handleClick}
                selectedKeys={[current]}
                mode="horizontal"
                theme="dark"
            >
                <Menu.Item
                    key="mail"
                    icon={<HomeOutlined />}
                >
                    <Link to="/">Home</Link>
                </Menu.Item>
                <Menu.Item
                    key="snippets"
                    icon={<MailOutlined />}
                >
                    <Link to="/snippets">Snippets</Link>
                </Menu.Item>
                {loggedIn &&
                    <Menu.Item
                        key="tags"
                        icon={<MailOutlined />}
                    >
                        <Link to="/my_snippets">My Snippets</Link>
                    </Menu.Item>
                }
                {false &&
                    <Menu.Item
                        key="tags"
                        icon={<MailOutlined />}
                    >
                        <Link to="/tags">Tags</Link>
                    </Menu.Item>
                }
                {loggedIn &&
                    <SubMenu
                        key="user"
                        icon={<UserOutlined />}
                        style={{ float: 'right' }}
                    >
                        <Menu.Item>
                            <Link to="/user">Profile</Link>
                        </Menu.Item>
                        <Menu.Item>
                            <Link to="/logout">Log out</Link>
                        </Menu.Item>
                    </SubMenu>
                    ||
                    <Menu.Item
                        key="login"
                        style={{ float: 'right', align: 'right', textAlign: 'right' }}
                    >
                        <Link to="/login">Log in</Link>
                    </Menu.Item>
                }
            </Menu>
        );
    }
}


function mapStateToProps(state) {
    const { loggedIn } = state.authentication;
    return {
        loggedIn
    };
}

const connectedPage = connect(mapStateToProps)(NavMenu);
export { connectedPage as NavMenu }; 