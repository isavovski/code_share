import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Form, Input, Button, Checkbox, Alert } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { userActions } from '../redux/actions';

class RegisterPage extends React.Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(values) {
        const { dispatch } = this.props;
        dispatch(userActions.register(values));
    }

    render() {
        const validateMessages = {
            required: "Email is required",
            types: {
                email: 'Not a valid email!',
            }
        };
        return (
            <>
                {
                    this.props.error &&
                    <Alert message={this.props.error} type="error" />
                }
                <div id="login-container">

                    <div
                        id="register-form"
                    >
                        <h1 style={{ color: "white" }}>
                            Register
                    </h1>
                        <Form
                            name="normal_login"
                            className="login-form"
                            initialValues={{
                                remember: true,
                            }}
                            validateMessages={validateMessages}
                            onFinish={this.handleSubmit}
                        >
                            <Form.Item
                                name={["name","first"]}
                                rules={[{required: true, message: 'Name is required'}]}
                            >
                                <Input
                                    placeholder="FirstName"
                                />
                            </Form.Item>
                            <Form.Item
                                name={["name","last"]}
                                rules={[{required: true, message: 'Last name is required'}]}
                            >
                                <Input
                                    placeholder="LastName"
                                />
                            </Form.Item>
                            <Form.Item
                                name="email"
                                rules={[
                                    {
                                        required: true,
                                        type: 'email'
                                    },
                                ]}
                            >
                                <Input
                                    placeholder="Email"
                                />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                rules={[
                                {
                                    required: true,
                                    message: 'Please input your password',
                                },
                                ]}
                            >
                                <Input.Password />
                            </Form.Item>

                            <Form.Item
                                name="confirm"
                                dependencies={['password']}
                                rules={[
                                {
                                    required: true,
                                    message: 'Please confirm your password',
                                },
                                ({ getFieldValue }) => ({
                                    validator(_, value) {
                                    if (!value || getFieldValue('password') === value) {
                                        return Promise.resolve();
                                    }
                                    return Promise.reject('Passwords do not match');
                                    },
                                }),
                                ]}
                            >
                                <Input.Password />
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" className="login-form-button">
                                    Sign up
                                </Button>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </>
        );
    };
}

function mapStateToProps(state) {
    const { loggingIn, error } = state.authentication;
    return {
        loggingIn,
        error
    };
}

const connected = connect(mapStateToProps)(RegisterPage);
export { connected as RegisterPage }; 