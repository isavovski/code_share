import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { snippetActions, userActions } from '../redux/actions';
import { Form, Input, Button, Checkbox, Tooltip } from 'antd';


import { Skeleton, Switch, Card, Avatar, Tag } from 'antd';
import { EditOutlined, EllipsisOutlined, LikeOutlined, CopyOutlined } from '@ant-design/icons';
import Highlight from "react-highlight";

const { Meta } = Card;

class Snippet extends React.Component {
    constructor(props) {
        super();
        this.state = {
            copied: false
        }
    }

    handleLike() {
        if (this.props.loggedIn) {
            this.props.dispatch(snippetActions.like(this.props.data._id));
        }
    }

    renderTags(tags = []) {
        return tags.map((tag, i) => (
            <Tag key={i}>{tag.name}</Tag>
        ));
    }

    render() {
        let { loggedIn, data, owned, user } = this.props;
        const mouseOut = () => {this.setState({copied: false})};
        let actions = [
            (
                loggedIn &&
                !data.likes.find(el => el == user.id) &&
                <span
                    onClick={this.handleLike.bind(this)}
                    style={{ userSelect: "none" }}
                >
                    <LikeOutlined key="like" />
                        Like | {data.likes.length}
                </span>
                ||
                <div
                    style={{ userSelect: "none", cursor: "default" }}
                >
                    {data.likes.length} Likes
                </div>
            ),
            (
                <div onClick={()=>{
                    navigator.clipboard.writeText(data.content)
                        .then(() => {
                            this.setState({copied: true})
                            
                        })
                }}>
                    <div
                        onMouseOut={() => mouseOut()}
                    >
                        <Tooltip 
                            visible={this.state.copied} 
                            title="Copied"
                        >
                            <CopyOutlined key="copy" />
                        </Tooltip>
                    </div>

                </div>
            )
        ];
        if (owned) {
            actions = actions.concat([
                // (
                //     <EditOutlined key="edit" />
                // )
            ]);
        }
        return (
            <Card
                className="snippet-card"
                actions={actions}
            >
                <Meta
                    title={data.title}
                />

                <Highlight className="snippet-code">
                    {data.content}
                </Highlight>
                <div style={{ textAlign: "start" }}>
                    {this.renderTags(data.tags)}
                </div>
            </Card>
        );
    }
}

function mapStateToProps(state) {
    const { loggedIn, user } = state.authentication;
    return {
        loggedIn,
        user
    };
}

const connectedPage = connect(mapStateToProps)(Snippet);
export { connectedPage as Snippet };