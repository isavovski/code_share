import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Form, Input, Button, Checkbox, Alert } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { userActions } from '../redux/actions';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            submitted: false
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(userActions.logout());
    }

    handleSubmit(values) {
        this.setState({ submitted: true });
        const { email, password } = values;
        const { dispatch } = this.props;
        if (email && password) {
            dispatch(userActions.login(email, password));
        }
    }

    render() {
        const validateMessages = {
            required: '${name} is required!',
            types: {
                email: 'Not a valid email!',
            }
        };
        return (
            <>
                {
                    this.props.error &&
                    <Alert message={this.props.error} type="error" />
                }
                <div id="login-container">

                    <div
                        id="login-form"
                    >
                        <h1 style={{ color: "white" }}>
                            Sign in
                    </h1>
                        <Form
                            name="normal_login"
                            className="login-form"
                            initialValues={{
                                remember: true,
                            }}
                            validateMessages={validateMessages}
                            onFinish={this.handleSubmit}
                        >
                            <Form.Item
                                name="email"
                                rules={[
                                    {
                                        required: true,
                                        type: 'email'
                                    },
                                ]}
                            >
                                <Input
                                    prefix={<UserOutlined className="site-form-item-icon" />}
                                    placeholder="Email"
                                />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                rules={[
                                    {
                                        required: true,
                                    },
                                ]}
                            >
                                <Input
                                    prefix={<LockOutlined className="site-form-item-icon" />}
                                    type="password"
                                    placeholder="Password"
                                />
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" className="login-form-button">
                                    Log in
                            </Button>
                                <div style={{ padding: "5px" }}></div>
                            <Button
                                type="primary"
                                onClick={() => {
                                    window.location= "/register";
                                }}
                                className="login-form-button">
                                    Register
                            </Button>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </>
        );
    };
}

function mapStateToProps(state) {
    const { loggingIn, error } = state.authentication;
    return {
        loggingIn,
        error
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage }; 