import React from 'react';
import { BrowserRouter as Router, Route, Link, useHistory, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { alertActions } from './redux/actions';
import { PrivateRoute } from './components/private/PrivateRoute';
import { HomePage } from './components/HomePage';
import { LoginPage } from './components/LoginPage';
import { SnippetsPage } from './components/SnippetsPage';
import { Button, Layout } from 'antd';
import { NavMenu } from './components/Menu';
import LogoutPage from './components/LogoutPage';
import { MySnippetsPage } from './components/private/MySnippetsPage';
import { RegisterPage } from './components/RegisterPage';

const { Header, Footer, Sider, Content } = Layout;

class App extends React.Component {
    constructor(props) {
        super(props);
        const { dispatch } = this.props;
    }

    render() {
        const { alert } = this.props;
        return (
            <Router>
                <Layout>
                    <Header>
                        <NavMenu/>
                    </Header>
                    <Content style={{backgroundColor: "#f8e9ff"}}>
                        <Switch>
                            <Route exact path="/" component={HomePage} />
                            <Route path="/login" component={LoginPage} />
                            <Route path="/logout" component={LogoutPage} />
                            <Route path="/register" component={RegisterPage} />
                            <Route path="/snippets" component={SnippetsPage} />
                            <PrivateRoute path="/my_snippets" component={MySnippetsPage} />
                        </Switch>
                    </Content>
                </Layout>
            </Router>
        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 